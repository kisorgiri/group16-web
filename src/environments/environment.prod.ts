export const environment = {
  production: true,
  BaseURL: 'http://localhost:8000/api/',
  ImgURL: 'http://localhost:8000/file/files/',
  SocketURL: 'http://localhost:8001'
};
