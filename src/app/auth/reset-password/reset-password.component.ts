import { Component, OnInit } from '@angular/core';
import { AuthSerivce } from '../services/auth.service';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  submitting: boolean = false;
  user;
  token;
  constructor(
    public authService: AuthSerivce,
    public msgService: MsgService,
    public router: Router,
    public activatedRoute: ActivatedRoute
  ) {
    this.user = new User({});
    this.token = this.activatedRoute.snapshot.params['token'];

  }

  ngOnInit() {
  }

  submit() {
    this.submitting = true;
    this.user.token = this.token;
    this.authService.resetPassword(this.user)
      .subscribe(
        (data) => {
          this.msgService.showInfo('Password reset sucessfully please login');
          this.router.navigate(['/auth/login']);
        }, error => {
          this.msgService.showError(error);
        }
      )
  }

}
