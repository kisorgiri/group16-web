import { Component, OnInit } from '@angular/core';
import { AuthSerivce } from '../services/auth.service';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  submitting: boolean = false;
  user;
  constructor(
    public authService: AuthSerivce,
    public msgService: MsgService,
    public router: Router
  ) {
    this.user = new User({});
  }

  ngOnInit() {
  }

  submit() {
    this.submitting = true;
    this.authService.forgotPassword(this.user)
      .subscribe(
        (data) => {
          this.msgService.showInfo('Password reset link sent to your email please check your inbox');
          this.router.navigate(['/auth/login']);
        },
        error => {
          this.submitting = false;
          this.msgService.showError(error);
        })
  }

}
