import { Component } from "@angular/core";
import { Router } from '@angular/router';
import { MsgService } from 'src/app/shared/services/msg.service';
import { AuthSerivce } from '../services/auth.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent {
    isDisabled: boolean = false;
    user;
    submitting: boolean = false;
    constructor(public router: Router,
        public msgService: MsgService,
        public authService: AuthSerivce) {
        this.user = new User({});
    }

    submit() {
        this.submitting = true;
        this.authService.register(this.user)
            .subscribe(
                data => {
                    this.msgService.showInfo('Registration successfull, Please login');
                    this.router.navigate(['/auth/login']);
                },
                error => {
                    this.submitting = false
                    this.msgService.showError(error);
                }
            )
    }
}



//data binding
// communication of view and controller
// 3 types of data binding
// 1 event binding ==> click,hover,change syntax (event_name)="expression to be called"
// 2 property binding ==> hide, disable ==> [propert_name] ="expression"
// 2 way data binding ==> synchronization of data between view and controller [(ngModel)]="data"

// state of form and elemnt
// pristine ===> form or element has not been interacted
// dirty===form or element value has been changed
// touched   interacted and out of foucs
// untouched ==> itneracted and within focus
// valid
// invalid
// error