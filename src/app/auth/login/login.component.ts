import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Observable } from 'rxjs';
import { AuthSerivce } from '../services/auth.service';
import { User } from 'src/app/shared/models/user.model';
import { SocketService } from 'src/app/shared/services/socket.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user;
  submitting: boolean = false;
  constructor(
    public router: Router,
    public msgService: MsgService, // service are injectable and should be injected in constructor to add in this of class
    public authService: AuthSerivce,
    public socketService: SocketService

  ) {
    this.user = new User({});
  }

  ngOnInit() {
    // component life cycle hook
  }

  watchJsCrashCourse() {
    let i = 1;
    let a = Observable.create((observer) => {
      setInterval(() => {
        observer.next('lecture ' + i); // success with value
        // observer.error('no content');

        if (i == 10) {
          observer.complete();
        }
        i++;
      }, 1000)
    })
    return a;
  }

  login() {
    this.submitting = true;
    this.authService.login(this.user)
      .subscribe(
        (data: any) => {
          console.log('data in >>>', data);
          localStorage.setItem('token', data.token);
          localStorage.setItem('user', JSON.stringify(data.user));
          this.msgService.showSuccess(`Welcome ${data.user.username}`);
          this.router.navigate(['/user/dashboard']);
        },
        (err) => {
          this.submitting = false;
          this.msgService.showError(err);
        });


  }
}
