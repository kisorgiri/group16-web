import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/shared/models/user.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthSerivce {
    url;
    constructor(public http: HttpClient) {
        this.url = environment.BaseURL + 'auth/'
    }

    login(data: User) {
        // return new Promise((resolve, reject) => {
        //     this.http.post(`${this.url}login`, data, {
        //         headers: new HttpHeaders({
        //             'Content-Type': 'application/json'
        //         })
        //     })
        //         .subscribe(
        //             (data) => {
        //                 resolve(data);
        //             },
        //             (err) => {
        //                 reject(err);
        //             })
        // })

        // return new Observable((observer) => {
        //     this.http.post(`${this.url}login`, data, {
        //         headers: new HttpHeaders({
        //             'Content-Type': 'application/json'
        //         })
        //     })
        //         .subscribe(
        //             (data) => {
        //                 observer.next(data);
        //             },
        //             (err) => {
        //                 observer.error(err);
        //             })
        // })
        return this.http.post(`${this.url}login`, data, this.getOptions())


    }

    register(data: User) {
        return this.http.post(`${this.url}register`, data, this.getOptions())
    }
    forgotPassword(data: User) {
        return this.http.post(`${this.url}forgot-password`, data, this.getOptions())

    }
    resetPassword(data: any) {
        return this.http.post(`${this.url}reset-password/${data.token}`, data, this.getOptions())

    }

    private getOptions() {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        }
    }
}