import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/auth/login',
        pathMatch: 'full'
    },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule'
    },
    {
        path: 'user',
        loadChildren: './users/users.module#UsersModule'
    },
    {
        path: 'item',
        loadChildren: './items/items.module#ItemsModule'
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }


]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}

// summarize
// routing navigation of component on application end point
// router module // should be imported with forRoot|| forChild method
// create separate file for routing module and inject that module to main module
// router config // this must be type Routes [{path:'a',component:acomponent}]
// forRoot should contain config

// directives
// router-outlet  place holder for every routing configuration
// routerLink (routing through template) this will be uses as  href attribute  specifying navigation end point

// controllerr navigation
// Router  this must be injected in constructor
// this.router.navigate([/end-point])

// data in routing
// query params ? optional ==>
// part of url /

// activatedRoute this is also injectable which must be injected
// this.activeROute.snapshot.params['field'] gives your dymanic endpoint value
// this.activeROuter.queryParams.subsribe((data)=>{
    // query  data here (? data)
// })

