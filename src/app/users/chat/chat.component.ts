import { Component, OnInit } from '@angular/core';
import { SocketService } from 'src/app/shared/services/socket.service';
import { MsgService } from 'src/app/shared/services/msg.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  msgBody;
  user;
  messages = [];
  userList = [];
  isTyping: boolean = false;
  typingUserInformation;
  constructor(
    public socketService: SocketService,
    public msgService: MsgService,
  ) {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.msgBody = {
      msg: '',
      senderName: this.user.username,
      senderId: '',
      receiverId: '',
      time: ''
    }
  }

  ngOnInit() {
    this.socketService.socket.emit('new-user', this.user.username);
    this.runSocket();
  }

  runSocket() {
    this.socketService.socket.on('hello', (data) => {
      console.log('bi directional communivatino between socket client and server >>>', data);
    });

    this.socketService.socket.on('reply-msg', (data) => {
      this.messages.push(data);
    });
    this.socketService.socket.on('reply-msg-user', (data) => {
      this.msgBody.receiverId = data.senderId;
      this.messages.push(data);
    });
    this.socketService.socket.on('users', (data) => {
      this.userList = data;
    })
    this.socketService.socket.on('typing', (data) => {
      this.isTyping = true;
      this.typingUserInformation = data.senderName;

    });
    this.socketService.socket.on('typing-stop', () => {
      this.isTyping = false;
    })
  }

  sendMsg() {
    if (!this.msgBody.receiverId) {
      return this.msgService.showInfo('please select user to continue');
    }
    this.userList.forEach((user, i) => {
      if (user.name === this.user.username) {
        this.msgBody.senderId = user.id;
      }
    })
    this.msgBody.time = new Date();
    this.socketService.socket.emit('new-msg', this.msgBody);
    this.msgBody.msg = '';
  }
  focusedIn(val) {
    console.log('val', val);
    if (val) {
      this.socketService.socket.emit('is-typing', this.msgBody);
    } else {
      this.socketService.socket.emit('is-typing-stop', this.msgBody);

    }
  }

  selectUser(user) {
    console.log('user is >>', user);
    this.msgBody.receiverId = user.id;

  }

}
