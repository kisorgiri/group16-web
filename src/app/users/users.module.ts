import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersRoutingModule } from './users.routing';
import { FormsModule } from '@angular/forms';
import { ChatComponent } from './chat/chat.component';



@NgModule({
  declarations: [ProfileComponent, DashboardComponent, ChatComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule
  ]
})
export class UsersModule { }
