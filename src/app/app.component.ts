import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MsgService } from './shared/services/msg.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'brodway infosys nepal';
  // class is controller of component
  constructor(public router: Router,
    public msgService: MsgService) {
    this.router.events.subscribe((nav: NavigationEnd) => {
      if (nav.url) {
        const url = nav.url.split('/')[1];
        if (url && url !== 'auth') {
          if (!localStorage.getItem('token')) {
            this.router.navigate(['/auth/login']);
            this.msgService.showInfo('Session expired please login again');
          }
        }
      }
    })
  }

  isLoggedIn() {
    return localStorage.getItem('token')
      ? true
      : false;

  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
}
// <app-root > </app-root>
// component consit of controller,template(html) and css

// meta data of component
// selector// 
// selector are those html element which can be used in view
// selector will carry entire component
// tempalteUrl or template it specifies view file(html file);
// styleUrls or styles it speciies the css fro component