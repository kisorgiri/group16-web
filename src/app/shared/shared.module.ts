import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MsgService } from './services/msg.service';
import { LoaderComponent } from './loader/loader.component';
import { SocketService } from './services/socket.service';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    LoaderComponent],
  imports: [
    CommonModule
  ],
  providers: [
    MsgService,
    SocketService], // providers have global scope
  exports: [HeaderComponent, FooterComponent, PageNotFoundComponent, LoaderComponent]
})
export class SharedModule { }
