import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ItemsModule } from './items/items.module';
import { SharedModule } from './shared/shared.module';
// NgModule is decorator which define class
// routing configuration here
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    // BrowserModule, //only root module will have browsermodule
    AppRoutingModule,
    AuthModule,
    UsersModule,
    ItemsModule,
    SharedModule,
    ToastrModule.forRoot()
  ],
  entryComponents: [],
  exports: [],
  providers: [],
  bootstrap: [AppComponent] // only root module will have bootsrap
  // any component that is kept in bootsrap section is root component
})
export class AppModule { }
// NOTE// there should not be any code between decorators and class
// any class below decorator is class refrenced by decorators
// this is main file

// glossary

// Module,
// component,
// service
// pipe
// directives
// all above mentioned terms are class in angular

// decorators
// decorator is function that is used to define class using meta -data
// every function with @prefix is decorator
// slectors
// meta data
// meta data is object that is used by decorator to define class


// meta data of NgModule decorators
// every property (key ) of meta data holds array
// the property and description are below
// declarations: declarations holds all the components,pipe and directives
// imports : // imports holds all the modules(module only)
// module can be of three types
// 1 inbuilt module(angular )
// 2 third party module (npm js ma paaune module)
// 3 own module(sub module)

// providers : services are kept on providers
// bootstrap // like we need root module angular requires root component to proceed.
// bootstrap property always cames in root module only
// component added in bootstrap is root component
// entryComponents //  j jati components routing configuration ma pardaina entry components ma rakhne eg (modal,dialog)
// exports // eg if i am working on submodule  and inject in root module anything that submodule likes to share(componetn, pipe,directives) that needs to be kept in exports
