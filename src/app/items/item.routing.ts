import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { AddItemComponent } from './add-item/add-item.component';
import { EditItemComponent } from './edit-item/edit-item.component';
import { ListItemComponent } from './list-item/list-item.component';
import { SearchItemComponent } from './search-item/search-item.component';
const itemRoutes: Routes = [
    {
        path: 'add',
        component: AddItemComponent
    },
    {
        path: 'edit/:id',
        component: EditItemComponent
    },
    {
        path: 'list',
        component: ListItemComponent
    },
    {
        path: 'search',
        component: SearchItemComponent
    }
]

@NgModule({
    imports: [RouterModule.forChild(itemRoutes)],
    exports: [RouterModule]
})
export class ItemRoutingModule {

}