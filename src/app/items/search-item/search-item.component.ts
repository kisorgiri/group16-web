import { Component, OnInit } from '@angular/core';
import { ItemService } from '../services/item.service';
import { Router } from '@angular/router';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Item } from 'src/app/shared/models/item.model';

@Component({
  selector: 'app-search-item',
  templateUrl: './search-item.component.html',
  styleUrls: ['./search-item.component.css']
})
export class SearchItemComponent implements OnInit {
  item;
  items: Array<any> = [];
  submitting: boolean = false;
  categories = [];
  names = [];
  allItems = [];
  constructor(
    public itemService: ItemService,
    public router: Router,
    public msgService: MsgService
  ) {
    this.item = new Item({});
    this.item.category = '';
  }

  ngOnInit() {
    this.itemService.search({})
      .subscribe(
        (data: any) => {
          this.allItems = data;
          this.allItems.forEach((item) => {
            if (this.categories.indexOf(item.category) == -1) {
              this.categories.push(item.category);
            }
          })
        },
        err => {
          this.msgService.showError(err);
        }
      )
  }

  submit() {
    this.submitting = true;
    this.itemService.search(this.item)
      .subscribe(
        (data: any) => {
          this.submitting = false;
          if (!data.length) {
            return this.msgService.showInfo('No any item matched your search query');
          }
          this.items = data;
          console.log('search result >>>', data);
        },
        err => {
          this.msgService.showError(err);
        })
  }
  resetSearch(val) {
    console.log('called from another component', val);
    this.items.length = 0;
    this.item = new Item({});
    this.item.category = '';
    this.names.length = 0;
  }

  categoryChanged(val) {
    this.item.name = '';
    this.names = this.allItems.filter((item) => {
      if (item.category == val) {
        return true;
      }
    })
  }

}
