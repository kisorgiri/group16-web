import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddItemComponent } from './add-item/add-item.component';
import { EditItemComponent } from './edit-item/edit-item.component';
import { ListItemComponent } from './list-item/list-item.component';
import { SearchItemComponent } from './search-item/search-item.component';
import { FormsModule } from '@angular/forms';
import { ItemRoutingModule } from './item.routing';
import { ItemService } from './services/item.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    AddItemComponent,
    EditItemComponent,
    ListItemComponent,
    SearchItemComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ItemRoutingModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [ItemService]
})
export class ItemsModule { }
