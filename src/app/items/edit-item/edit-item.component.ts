import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemService } from '../services/item.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent implements OnInit {
  id;
  item;
  isSubmitting: boolean = false;
  isLoading: boolean = false;
  imgURL;
  fileToUpload = [];
  constructor(
    public msgService: MsgService,
    public router: Router,
    public itemService: ItemService,
    public activeRoute: ActivatedRoute
  ) {
    this.id = this.activeRoute.snapshot.params['id'];
    this.imgURL = environment.ImgURL;
    // constructor code block run first
    console.log('id >>', this.id);
  }

  ngOnInit() {
    this.isLoading = true;
    this.itemService.getById(this.id)
      .subscribe(
        (data: any) => {
          this.isLoading = false;
          console.log('data is >>>', data);
          this.item = data;
          if (data.discount) {
            this.item.discountedItem = data.discount.discountedItem;
            this.item.discountType = data.discount.discountType;
            this.item.discountUnit = data.discount.discountUnit;
          }
        }, error => {
          this.isLoading = false;
          this.msgService.showError(error);
        });
  }

  submit() {
    this.isSubmitting = true;
    this.itemService.upload(this.item, this.fileToUpload, 'PUT')
      .subscribe((data) => {
        this.msgService.showInfo('Item updated successfuly');
        this.router.navigate(['/item/list']);
      }, (err) => {
        this.isSubmitting = false;
        this.msgService.showError(err);
      })
  }

  fileChanged(ev) {
    this.fileToUpload = ev.target.files;
  }

}
