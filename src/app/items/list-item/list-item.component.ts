import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ItemService } from '../services/item.service';
import { Router } from '@angular/router';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Item } from 'src/app/shared/models/item.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  items: any;
  isLoading: boolean = false;
  imgUrl;
  @Input() inputData;
  @Output() resetSearch = new EventEmitter();
  title: string = 'View Items';
  constructor(
    public itemService: ItemService,
    public router: Router,
    public msgService: MsgService
  ) {
    this.imgUrl = environment.ImgURL;
    console.log('input data will not apperar here', this.inputData)
  }

  ngOnInit() {
    console.log('input data of component', this.inputData);
    if (this.inputData) {
      this.title = "Search Results"
      this.items = this.inputData;
      return;
    }
    this.isLoading = true;
    this.itemService.get()
      .subscribe(
        (data) => {
          this.isLoading = false;
          this.items = data || [];
        },
        (err) => {
          this.isLoading = false;
          this.msgService.showError(err);
        }
      )
  }

  remove(id, index) {
    let confirmation = confirm('Are you sure to remove?');
    if (confirmation) {
      this.itemService.remove(id)
        .subscribe(
          (data: any) => {
            this.msgService.showInfo('Item Deleted');
            this.items.splice(index, 1);
          },
          error => {
            this.msgService.showError(error);
          }
        )
    }
  }
  searchAgain() {
    console.log('search again called');
    this.resetSearch.emit('hi');
    // this.should communicate search component
  }

}
