import { Component, OnInit } from '@angular/core';
import { Item } from 'src/app/shared/models/item.model';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Router } from '@angular/router';
import { ItemService } from '../services/item.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {
  item;
  submitting: boolean = false;
  filesToUpload = [];
  constructor(
    public msgService: MsgService,
    public router: Router,
    public itemService: ItemService
  ) {
    this.item = new Item({});
  }

  ngOnInit() {
    console.log('item >>', this.item);
  }

  submit() {
    this.submitting = true;
    this.itemService.upload(this.item, this.filesToUpload, 'POST')
      .subscribe(
        (data) => {
          this.msgService.showSuccess('Item added susscessfully');
          // this.item = new Item({});
          this.router.navigate(['/item/list']);
        },
        (err) => {
          this.msgService.showError(err);
          this.submitting = false;
        }
      )
  }

  fileChanged(event) {
    this.filesToUpload = event.target.files;
  }

}
