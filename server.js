const express = require("express");
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, '/dist/group16-web/')));

app.get('/*', function (req, res, next) {
    res.sendfile(path.join(__dirname, 'dist/group16-web/index.html'))
})
app.listen(9090);


